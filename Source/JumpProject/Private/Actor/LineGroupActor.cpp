#include "Actor/LineGroupActor.h"
#include "Components/StaticMeshComponent.h"
#include "../GameConstants.h"

ALineGroupActor::ALineGroupActor()
{
    static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_CUBE(
        TEXT("/Script/Engine.StaticMesh'/Engine/BasicShapes/Cube.Cube'"));

    static ConstructorHelpers::FObjectFinder<UMaterial> MASTER_COLOR(
        TEXT("/Script/Engine.Material'/Game/Resources/Master_Color.Master_Color'"));
    
    UStaticMesh *cubeStaticMesh = nullptr;
    UMaterial * lineObjectMaterial = nullptr;
    if (SM_CUBE.Succeeded()) cubeStaticMesh = SM_CUBE.Object;
    else UE_LOG(LogTemp, Error, TEXT("SM_CUBE is not loaded!"));

    if (MASTER_COLOR.Succeeded()) lineObjectMaterial = MASTER_COLOR.Object;
    else UE_LOG(LogTemp, Error, TEXT("MASTER_COLOR is not loaded!"));

	PrimaryActorTick.bCanEverTick = true;

    InitializeLineObjects(cubeStaticMesh, lineObjectMaterial);
}

void ALineGroupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void ALineGroupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // 라인 그룹을 이동시킵니다.
    MoveLineGroup(DeltaTime);

    // 라인 오브젝트를 스크롤링 시킵니다.
    ScrollingLineObject(DeltaTime);

}

void ALineGroupActor::InitializeLineObjects(
    UStaticMesh* lineObjectStaticMesh, 
    UMaterial* lineObjectMaterial)
{
    // 루트 컴포넌트를 생성합니다.
    LineGroupRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT_COMPONENT"));
    SetRootComponent(LineGroupRootComponent);

    // 라인 오브젝트 크기Y
    float lineObjectTerm = FGameConstants::GetLineObjectTerm();

    // 사용되는 라인 오브젝트 개수
    int32 numberOfLineObject = FGameConstants::NUMBER_OF_LINEOBJECT;

    // 라인 오브젝트 배치 시작 위치
    FVector lineObjectStartLocation = 
        ((lineObjectTerm * numberOfLineObject * 0.5) - (lineObjectTerm * 0.5f)) * FVector::LeftVector;

    for (int32 i = 0; i < FGameConstants::NUMBER_OF_LINEOBJECT; ++i)
    {  
        // Static Mesh Component 를 생성합니다.
        UStaticMeshComponent* newLineObject = CreateDefaultSubobject<UStaticMeshComponent>(
            *(FString("LINEOBJ") + FString::FromInt(i)));

        //  생성된 Static Mesh Component 의 부모 컴포넌트를 지정합니다.
        newLineObject->SetupAttachment(LineGroupRootComponent);

        if (lineObjectStaticMesh != nullptr)
        {
            // 라인 오브젝트의 Static Mesh 에셋을 설정하는 구문
            newLineObject->SetStaticMesh(lineObjectStaticMesh);
        }

        if (lineObjectMaterial != nullptr)
        {
            // 라인 오브젝트의 메터리얼을 설정하는 구문
            newLineObject->SetMaterial(0, lineObjectMaterial);
        }

        // 라인 오브젝트의 크기를 설정하는 구문
        newLineObject->SetRelativeScale3D(FGameConstants::GetLineObjectSize());

        // 라인 오브젝트를 일렬로 나열시키는 구문
        newLineObject->SetRelativeLocation(
            FVector::RightVector * FGameConstants::GetLineObjectTerm() * i + lineObjectStartLocation);

        // 생성된 Static Mesh Component 를 배열에 추가합니다.
        LineObjects.Add(newLineObject);

    }
}

void ALineGroupActor::SetLineObjectColors(
    TArray<FLinearColor> colors, 
    TArray<EColorType> colorTypes)
{
    for (int32 i = 0; i < LineObjects.Num(); ++i)
    {
        // i 번째 LineObject 를 얻습니다.
        UStaticMeshComponent* lineObject = LineObjects[i];

        // 설정시킬 색상값을 얻습니다.
        EColorType colorType = colorTypes[i];
        FLinearColor lineColor = colors[(int32)colorType];

        // 통과 가능한 색상 타입이라면
        if (PassableColorType == colorType)
        {
            // 통과 가능 LineObject 인덱스를 설정합니다.
            PassableLineObjectIndex = i;
        }

        // 라인 오브젝트에 색상을 설정합니다.
        // 메터리얼 파라미터들이 다른 값을 갖도록 하기 위하여
        // 메터리얼을 복사 생성합니다.
        UMaterialInstanceDynamic* materialDynamicInstance = UMaterialInstanceDynamic::Create(
            lineObject->GetMaterial(0), this);

        // 메터리얼 파라미터를 변경합니다.
        materialDynamicInstance->SetVectorParameterValue(
            TEXT("_Color"), lineColor);

        lineObject->SetMaterial(0, materialDynamicInstance);
    }

}

void ALineGroupActor::MoveLineGroup(float deltaTime)
{
    // 라인 그룹 텀을 얻습니다.
    float lineGroupTerm = FGameConstants::GetLineGroupTerm();

    // 라인 그룹을 이동시킬 목표 위치를 계산합니다.
    FVector newLocation = GetActorLocation();
    newLocation.Z = ((LineGroupIndex * lineGroupTerm) + lineGroupTerm) * -1;

    // 선형 보간 함수
    // - 끝점의 값이 주어졌을 때 그 사이에 위치한 값을 추청하기 위하여 
    //   직선 거리에 따라 선형적으로 계산하는 방법
    // - A : 시작점
    // - B : 끝점
    // - Alpha : 
    newLocation.Z = FMath::Lerp(
        GetActorLocation().Z, 
        newLocation.Z, 
        FGameConstants::GetLineGroupMoveSpeed() * deltaTime);

    // 액터의 위치를 설정합니다.
    SetActorLocation(newLocation);
}

void ALineGroupActor::ScrollingLineObject(float deltaTime)
{
    // 라인 오브젝트 텀
    const float lineObjectTerm = FGameConstants::GetLineObjectTerm();

    // 최대 스크롤링 거리
    const float maxScrollingDistance = 
    (LineObjects.Num() * lineObjectTerm) + (lineObjectTerm * 0.5f) - (LineObjects.Num() * lineObjectTerm * 0.5f);



    for (UStaticMeshComponent* lineObj : LineObjects)
    {
        // 라인 오브젝트의 상대 위치를 얻습니다.
        FVector lineLocation = lineObj->GetRelativeLocation();

        // 오른쪽으로 이동시킵니다.
        lineLocation.Y += FGameConstants::GetLineObjectScrollingSpeed() * deltaTime;

        // 라인 오브젝트 무한 스크롤링 구문
        // 최대 스크롤링 거리를 초과했다면
        if (lineLocation.Y > maxScrollingDistance) 
            lineLocation.Y -= (LineObjects.Num() * lineObjectTerm);

        // 라인 오브젝트 위치를 설정합니다.
        lineObj->SetRelativeLocation(lineLocation);
    }

}

void ALineGroupActor::InitializeLineGroup(
    int32 index,
    TArray<FLinearColor> colors,
    TArray<EColorType> colorTypes,
    EColorType passableColor,
    EColorType nextColor,
    FLineGroupPassedEventSignature lineGroupPassedEvent)
{
    PassableColorType = passableColor;
    NextColorType = nextColor;

    NextColor = colors[(int32)NextColorType];

    // 라인 인덱스를 설정합니다.
    SetLineGroupIndex(index);

    // 라인 오브젝트 색상들을 모두 설정합니다.
    SetLineObjectColors(colors, colorTypes);

    // 라인 그룹 통과 이벤트 설정
    LineGroupPassedEvent = lineGroupPassedEvent;
}

bool ALineGroupActor::IsPassableLineObject(UStaticMeshComponent* lineObject)
{
    // lineObject 에 해당하는 요소 인덱스를 얻습니다.
    int32 index = LineObjects.Find(lineObject);

    // 통과 가능여부를 반환합니다.
    return index == PassableLineObjectIndex;
}

void ALineGroupActor::OnLineGroupPassed()
{
    // 라인 그룹 통과 이벤트를 발생시킵니다.
    LineGroupPassedEvent.ExecuteIfBound(LineGroupIndex);
}

