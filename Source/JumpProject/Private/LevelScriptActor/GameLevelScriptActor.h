#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "../Enum/ColorType.h"
#include "GameLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class AGameLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

protected :
    UPROPERTY(EditAnywhere)
    int32 InitialLineGroupCount;

private :
    // 색상 정보를 가지는 데이터 테이블 에셋
    class UDataTable* DT_ColorType;

    // 색상 정보들을 가지고있는 맵
    TMap<EColorType, FLinearColor> ColorDatas;

    // 사용 가능한 색상들을 가지는 배열
    TArray<FLinearColor> Colors;

    // 생성시킬 라인 그룹 액터 클래스입니다.
    TSubclassOf<class ALineGroupActor> BP_LineGroupActorClass;

    // 생성된 라인의 정답 색상을 저장시키기 위한 변수입니다.
    EColorType NextPassableColor;

    // 생성된 라인 객체들을 나타냅니다.
    TArray<class ALineGroupActor*> GeneratedLines;
public :
    AGameLevelScriptActor();

protected :
    virtual void BeginPlay() override;

private :
    // 라인 그룹을 생성합니다.
    // lineGroupIndex : 라인 그룹 인덱스를 전달합니다.
    // return : 생성된 라인 그룹 객체를 반환합니다.
    class ALineGroupActor* GenerateLineGroup(int32 lineGroupIndex);

    // 랜덤한 색상을 반환합니다.
    EColorType GetRandomColorType() const;

    // 라인 그룹에서 사용될 랜덤하게 섞인 색상 타입 배열을 반환합니다.
    TArray<EColorType> GetSuffledColorTypeArray(EColorType inclusiveColor);

    // 두 색상값을 스왑시킵니다.
    void Swap(EColorType& ref_a, EColorType& ref_b);

    // 라인 그룹을 통과한 경우 호출되는 메서드입니다.
    // passedLineGroupIndex : 통과된 라인 그룹 인덱스가 전달됩니다.
    void OnLineGroupPassed(int32 passedLineGroupIndex);

    // 라인 그룹을 제거합니다.
    // lineGroupIndex : 배열에서 제외시킬 라인 그룹 인덱스를 전달합니다.
    void RemoveLineGroup(int32 lineGroupIndex);
    
    // 라인 그룹을 생성하여 맨 뒤로 보냅니다.
    void PushBackLineGroup();

public :
    // 색상 타입에 따라 색상값을 반환합니다.
    FLinearColor GetColorData(EColorType colorType) const;
    

    

	
};
