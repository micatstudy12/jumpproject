#include "PlayerController/GamePlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "Actor/PlayerPawn.h"

// 카메라 액터 태그
#define TAG_CAMERA              TEXT("Camera")


void AGamePlayerController::BeginPlay()
{
    Super::BeginPlay();

    FindCameraComponent();
}

void AGamePlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction(
        TEXT("Jump"), 
        EInputEvent::IE_Pressed, 
        this, 
         &ThisClass::OnJumpKeyPressed);
    // BindAction(
    //   const FName ActionName, 
    //   우리가 등록시킨 키 이름
    // 
    //   const EInputEvent KeyEvent, 
    //   키 이벤트 타입 (키가 눌렸을 때, 떼어졌을 때)
    // 
    //   UserClass* Object, 
    //   이벤트를 실행하기 위한(멤버 함수를 호출하기 위한) 객체
    // 
    //   typename FInputActionHandlerSignature::TMethodPtr< UserClass > Func)
    //   Func : 이 키가 입력되었을 때 호출될 멤버 함수
    //   void() 형식이 바인딩될 수 있습니다.

}

void AGamePlayerController::FindCameraComponent()
{
    // 카메라 액터를 찾아 저장하기 위한 배열
    TArray<AActor*> findActors;

    // TAG_CAMERA Tag 를 갖는 액터를 월드에서 찾습니다.
    UGameplayStatics::GetAllActorsWithTag(GetWorld(), TAG_CAMERA, findActors);
    // UGameplayStatics : 게임 플레이 중에 사용 가능한 유용한 메서드들을 제공하는 정적 클래스
    // GetAllActorsWithTag(WorldContextObject, Tag, OutActors)
    // Tag 를 갖는 액터를 WorldContextObject 에서 찾아 OutActors 배열에 추가합니다.

    // 찾은 카메라 액터로 뷰를 설정합니다.
    SetViewTargetWithBlend(findActors[0]);
}

void AGamePlayerController::OnJumpKeyPressed()
{
    // 조종중인 Pawn 객체를 얻습니다.
    Cast<APlayerPawn>(GetPawn())->OnJump();
}
