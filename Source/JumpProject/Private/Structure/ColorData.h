#pragma once
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "../Enum/ColorType.h"
#include "ColorData.generated.h"



// BlueprintType : 이 구조체를 블루프린트에서 변수로 사용할 수 있는 유형으로 설정합니다.
USTRUCT(BlueprintType)
struct FColorData : public FTableRowBase
{
    GENERATED_USTRUCT_BODY()

public:
    // 색상 타입
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EColorType ColorType;

    // 색상 값
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FLinearColor Color;
};
